(function() {
    'use strict';
    angular.module(appName).config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $qProvider, $authProvider) {
        $stateProvider.state('home', {
            url: '/',
            templateUrl: 'partials/home.html',
            controller: 'LoginCtrl'
        }).state('home.list', {
            url: 'list',
            templateUrl: 'partials/home-list.html',
            controller: function($scope) {
                $scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
            }
        }).state('home.paragraph', {
            url: 'paragraph',
            template: 'I could sure use a drink right now.'
        }).state('about', {
            url: '/about',
            permission: 'Edit',
            views: {
                '': {
                    templateUrl: 'partials/about.html'
                },
                'columnTwo@about': {
                    template: 'Look I am a column!'
                },
                'columnOne@about': {
                    template: 'partials/table-data.html',
                    // controller: 'scotchController'
                }
            }
        });
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
        // $httpProvider.interceptors.push('securityInterceptor');
        $httpProvider.interceptors.push('authInterceptor');
        $qProvider.errorOnUnhandledRejections(false);
        $authProvider.facebook({
            clientId: '617214758448227',
            url: '/auth.php',
            // responseType: 'token',
            redirectUri: location.origin + location.pathname
        });
        
    });
}());