(function() {
	'use strict';
	// Auth Interceptor
    angular.module(appName).factory('authInterceptor', function($q, $window, $location) {
        return {
            request: function(config) {
                if ($window.sessionStorage.access_token) {
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
                }
                return config;
            },
            requestError: function(rejection) {
                alert("Something went wrong");
                return $q.reject(rejection);
            },
            responseError: function(rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    $location.path('/unauthorized').replace();
                }
                return $q.reject(rejection);
            }
        };
    });
})();