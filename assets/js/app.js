(function() {
    'use strict';
    var app = angular.module(appName, ['ui.router','satellizer']),
        permissionList;
    angular.element(document).ready(function() {
        $.getJSON('/data/getperm.json', function(data) {
            permissionList = data;
            // console.log(permissionList);
            angular.bootstrap(document, [appName]);
        });
    });
    app.run(function(permissions) {
        permissions.setPermissions(permissionList);
        // console.log(permissionList);
    });
    
    // app.config(function($authProvider) {
    //     $authProvider.facebook({
    //         clientId: '617214758448227'
    //     });
    //     // Optional: For client-side use (Implicit Grant), set responseType to 'token' (default: 'code')
    //     // $authProvider.facebook({
    //     //     clientId: '617214758448227',
    //     //     responseType: 'token'
    //     // });
    //     $authProvider.google({
    //         clientId: 'Google Client ID'
    //     });
    //     $authProvider.github({
    //         clientId: 'GitHub Client ID'
    //     });
    //     $authProvider.linkedin({
    //         clientId: 'LinkedIn Client ID'
    //     });
    //     $authProvider.instagram({
    //         clientId: 'Instagram Client ID'
    //     });
    //     $authProvider.yahoo({
    //         clientId: 'Yahoo Client ID / Consumer Key'
    //     });
    //     $authProvider.live({
    //         clientId: 'Microsoft Client ID'
    //     });
    //     $authProvider.twitch({
    //         clientId: 'Twitch Client ID'
    //     });
    //     $authProvider.bitbucket({
    //         clientId: 'Bitbucket Client ID'
    //     });
    //     $authProvider.spotify({
    //         clientId: 'Spotify Client ID'
    //     });
    //     // No additional setup required for Twitter
    //     $authProvider.oauth2({
    //         name: 'foursquare',
    //         url: '/auth/foursquare',
    //         clientId: 'Foursquare Client ID',
    //         redirectUri: window.location.origin,
    //         authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
    //     });
    // });
}());