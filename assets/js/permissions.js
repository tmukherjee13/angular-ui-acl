(function() {
    'use strict';
    angular.module(appName).factory('permissions', function($rootScope) {
        var permissionList;
        return {
            setPermissions: function(permissions) {
                permissionList = permissions;
                $rootScope.$broadcast('permissionsChanged');
            },
            hasPermission: function(permission) {
            	// console.log(permission);
            	// console.log(permissionList);
            	
                permission = permission.trim();
                return _.some(permissionList, function(item) {
                    if (_.isString(item.Name)) {
                        return item.Name.trim() === permission;
                    }
                });
            }
        };
    });
})();