(function() {
    'use strict';
    angular.module(appName).controller('mainAppCtrl', function($scope, $location, permissions) {
        $scope.$on('$stateChangeStart', function(event, next, toParams, current, fromParams) {
            if (next.permission !== undefined) {
                var permission = next.permission;
                if (_.isString(permission) && !permissions.hasPermission(permission)) {
                    $location.path('paragraph').replace();
                }
            }
        });
    });
})();