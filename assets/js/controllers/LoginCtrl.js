(function() {
    'use strict';
    angular.module(appName).controller('LoginCtrl', function($scope, $location, permissions, $auth) {
        $scope.authenticate = function(provider) {
            $auth.authenticate(provider);
        };
    });
})();